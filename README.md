AGRO-TRACKING




ABSTRACT
Agro-Tracking is a simple application uses blockchain, implemented using Sawtooth framework. In the day to day world the safety of the food products is a matter of question, the blockchain can give a great solution to this problem by traceability. The most common use of blockchain in food and agriculture supply chains is to improve traceability. It enables companies to quickly track unsafe products back to their source and see where else they have been distributed. This can prevent illness and save lives, as well as reducing the cost of product recalls
Agro-tracking is such an application that can provide tracking options, track the product to which hands it have reached and if it’s found any problems in its quality the product can be easily tracked and necessary steps can be taken to ensure the safety of the customer. Hence Agro-tracking is a way to ensure food safety.



INSTALLATION GUIDELINES
1. The first dependency we need is Node Package Manager or NPM, which comes with node js. To know whether node js is installed just go to terminal and type 
$node -v

2. The second dependency is Express, Express is a minimal and flexible Node.js web application framework Helps develop backend servers In web applications

		$ npm install -g express-generator
		$ express –-view= hbs


3. Next need to install node modules:

	$ npm install

4. The third dependency is Docker &Containers
Docker: Tool designed to make it easier to create, deploy, and run applications by using containers Docker is a bit like a virtual machine Unlike a VM, rather than creating a whole virtual OS, Docker allows Apps to use the same OS as the system


Docker images: A template of instructions which is used to create containers It is created by the sequence of commands written in a file called as Dockerfile
Docker container: It is a standard unit of software that packages up code and all its dependencies Container images become Container containers at runtime
Docker compose: it is a tool for defining and running multi-container Docker applications you use a YAML file to configure your application’s services with a single command, you create and start all the services from your configuration.

5. Once all the dependency are installed and coded ,then it can be run using the command
		$ sudo docker-compose up
to exit
		$sudo  docker-compose down
