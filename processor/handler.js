// 'use strict'

/* Transaction Processor */
const {TextDecoder,TextEncoder} = require('text-encoding/lib/encoding')
const { TransactionHandler } = require('sawtooth-sdk/processor/handler')
const { TransactionProcessor } = require('sawtooth-sdk/processor');
const crypto = require('crypto');
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1')
const {createContext,CryptoFactory} = require('sawtooth-sdk/signing');
const protobuf =require('sawtooth-sdk/protobuf')

//family name
const FAMILY_NAME='Agro_Tracking';
const NAMESPACE=hash(FAMILY_NAME).substring(0,6);
console.log("****************",NAMESPACE)
var decoder = new TextDecoder('utf8')
var encoder = new TextEncoder('utf8')
PRODUCERKEY = '0b845ff442ffbba9652ef1078289f7cc0316a54c62649465b36e4f322f89452b'

// function to hash data
function hash(data) {
  return crypto.createHash('sha512').update(data).digest('hex');
}

/* function to write data to state 
parameter : 
    context -  validator context object
    address - address to which data should be written to
    data - the data tto be written
*/
function writeToStore(context, address, data){
    // console.log("*********",address);
    // console.log("data**********");
    // console.log("data*************",data[0]);
    // console.log("data*************",data[1]);
    // console.log("data*************",data[2]);
    // console.log("data*************",data[3]);
    // console.log("data*************",data[4]);
    let entries={};
   dataBytes = encoder.encode(data)
     entries = {
    [address]: dataBytes
  }
  // console.log("entries********",entries);
return context.setState(entries,this.timeout);

}


/* function to retrive the address of a particular product based on its product Id */

function getProductAddress(productId){
  const context = createContext('secp256k1');
  let key = Secp256k1PrivateKey.fromHex(PRODUCERKEY)
  let signer = new CryptoFactory(context).newSigner(key);
  let publicKeyHex = signer.getPublicKey().asHex()    
  let keyHash  = hash(publicKeyHex)
  let nameHash = hash("Agro_Tracking")
  let proIdHash = hash(productId)
 
  return nameHash.slice(0,6)+proIdHash.slice(0,6)+keyHash.slice(0,58)

}


  


/* function to add product data to chain
parameter :
context - validator context object
producer - name of producer
productId - The unique product number
producerId:The unique producer number
itemName: The name of the item
retailerId:The unique number for retailer
dop:Date of Product given for production
doe:The expiary date of the product
quantity:The measure of product sold to the reatailer by producer 
 */ 
     

function addProduct (context,producer,productId,producerId,itemName,retailerId,dop,doe,quantity) {
    let product_Address = getProductAddress(productId)
   console.log("**********************",product_Address)
    let product_detail =[producer,productId,producerId,itemName,retailerId,dop,doe,quantity]
    // console.log("########################")
    // console.log("producer",product_detail[0]);
    // console.log("productId",product_detail[0]);
    // console.log("producerID",product_detail[0]);
    // console.log("itemname",product_detail[0]);
    // console.log("reatailerid",product_detail[0]);
    // console.log("dop",product_detail[0]);
    // console.log("doe",product_detail[0]);
    //  console.log("quantity====",product_detail[0]);
    // console.log("add event****************************");
    context.addEvent(
      'Producer-AddProduct/productSoldToRetailer',
      [['prodict_Id',productId],
      ['Producer_Id',producerId],
      ['Item_Name',itemName],
      ['Retailer_Id',retailerId],
      ['DateOfProduction',dop],
      ['DateOfExpiary',doe],
      ['Quantity',quantity]
    ],null);

    return writeToStore(context,product_Address,product_detail)
}

/* function to add  Retailer registration data to chain
parameter :
context - validator context object

 productId - The unique product number
 customerId- The unique number for the customer
phoneNo- The mobile number of the customer
 dos- the date of sale of product by retailer
 itemName: The name of the item
quantity- yhe measure of quantity
 */


function retailProduct(context,productIdR,customerId,phoneNo,itemNameR,dos,Retailer,quantityR){
    console.log("selling the product")
    let address = getProductAddress(productIdR)
    console.log("address:*********",address)
    return context.getState([address]).then(function(data){
    // console.log("data*************",JSON.stringify(data));

  if(data[address] == null || data[address] == "" || data[address] == []){
        console.log("********Invalid ProductID! The Product Id Does Not Exist*************")
    }else{
    let stateJSON = decoder.decode(data[address])
    // console.log("**************stateJSON",stateJSON)
    let newData = stateJSON + "," + [customerId,phoneNo,itemNameR,dos,Retailer,quantityR].join(',')
   console.log("add event****************************");
   context.addEvent('Retailer-AddProduct/ProductSoldToCustomer',
   [['Product_Id',productIdR],
   ['Customer_Id',customerId],
   ['PhoneNo',phoneNo],
   ['Item_Name',itemNameR],
   ['DateOfSale',dos],
   ['Quantity_of Sale',quantityR]

],null);
   
    return writeToStore(context,address,newData)
    }
    })
        

    
}


//transaction handler class

class Product extends TransactionHandler{
    constructor(){
        super(FAMILY_NAME, ['1.0'], [NAMESPACE]);
    

    }
//apply function
    apply(transactionProcessRequest, context){
        let PayloadBytes = decoder.decode(transactionProcessRequest.payload)
        let Payload = PayloadBytes.toString().split(',')
        let action = Payload[0]
        // console.log("*****Inside Apply");
        // console.log("payload[0]-----",Payload[0]);
       
        if (action == "Add product"){
            // console.log("*****Apply Add Product********")
            // console.log("*************")
            // console.log("payload",Payload[1]);
            // console.log("payload",Payload[2]);
            // console.log("payload",Payload[3]);
            // console.log("payload",Payload[4]);
            // console.log("payload",Payload[5]);
            // console.log("payload",Payload[6]);
     
            return addProduct(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6],Payload[7],Payload[8])
           
        }
        else if(action === "Register Details"){
    //         console.log("*************")
    //    console.log("payload",Payload[1]);
    //    console.log("payload",Payload[2]);
    //    console.log("payload",Payload[3]);
    //    console.log("payload",Payload[4]);
    //    console.log("payload",Payload[5]);
    //    console.log("payload",Payload[6]);
            return retailProduct(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6],Payload[7])
        }
        
    }
}

module.exports = Product;



