function viewData()
{
    window.location.href='listView';
}

/*functiom to get the address*/

function getProductAddress(productId,ProducerName){
  let keyHash  = hash(getUserPublicKey(ProducerName))
    let nameHash = hash("Agro Chain")
    let proIdHash = hash(productId)
   return nameHash.slice(0,6)+proIdHash.slice(0,6)+keyHash.slice(0,58)
  
  }


/*function to add the product by producer */

function addProductAsProducer(event)
{
    event.preventDefault();
    let privKey = document.getElementById('producerPrivKey').value;
    let proId =document.getElementById('productId').value;
    let producId=document.getElementById('producerId').value;
    let itemNam=document.getElementById('itemName').value;
    let retailID=document.getElementById('retailerId').value;
    let dop=document.getElementById('dop').value;
    let doe=document.getElementById('doe').value;
    let dopUpdated = new Date(dop.replace(/-/g,'/'));  
    let doeUpdated = new Date(doe.replace(/-/g,'/'));
    let quantity=document.getElementById('quantity').value;
   
    // console.log("***********main.js*********")
    // console.log("privKey**********",privKey);
    // console.log("productId*************",proId);
    // console.log("producerId*************",producId);
    // console.log("ItemName*************",itemNam);
    // console.log("reatilId*************",retailID);
    // console.log("dop*************",dop);
    // console.log("doe*************",doe);
    // console.log("quantity*************",quantity);
// validations
if (privKey.length==''||proId.length==''||producerId.length==''||itemNam.length==''||retailID.length==''||dop.value== ''||doe.value==''||quantity.length === 0)
    {
      alert("Please Check all The Details Data Is Empty!!!!") ;
    }
else if(!proId.match(/^[0-9a-zA-Z]+$/))
    {

      alert("Please provide correct product ID")

    }
else if(!producId.match(/^[0-9a-zA-Z]+$/))
    {
       alert("Please provide correct Producer ID")
  
    }
else if(!itemNam.match(/^[a-zA-Z]+$/))
    {
       alert("Please provide correct Item Name")
    }
else if(!retailID.match(/^[0-9a-zA-Z]+$/))
    {
        alert("Please provide correct retailId")

    }
else if(!dop.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/))
    {
        alert("please check the Date")
    }
else if(!doe.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/))
    {
        alert("please check the Date")
    }
else if(dopUpdated > doeUpdated)
    {
        alert("Please Check the Dates the Expiary date doesnt Match!!!")
    }
                                          
else if(!quantity.match(/^[1-9][0-9]*[a-zA-z]+$/))
    {
        alert("Please check the quantity number!!")

    }
  

else{
    $.post('/addProduct',{ Pkey:privKey,productId:proId,producerId:producId,itemName:itemNam,retailerId:retailID,dop:dop,doe:doe,quantity:quantity },
    'json');
    alert("Products Are Added!!!");
    document.getElementById('manform').reset();
    return true;
    }

}

/*function to add the product by Retailer */
function addProductAsRetailer(event)
{
  
    event.preventDefault();
    let privKey = document.getElementById('retailerPrivKey').value;
    let proId =document.getElementById('productIdR').value;
    let custId=document.getElementById('customerId').value;
    let phoneNum=document.getElementById('phoneNo').value;
    let dos=document.getElementById('dos').value;
    let itemNam=document.getElementById('itemNameR').value;
    let quantity=document.getElementById('quantityR').value;
    // console.log("***********main.js*********")
    // console.log("privKey**********",privKey);
    // console.log("productId*************",proId);
    // console.log("custId*************",custId);
    // console.log("phoneNum*************",phoneNum);
    // console.log("ItemName*************",itemNam);
    // console.log("dos*************",dos);
    // console.log("quantity*************",quantity);
   
   //validations
    if(privKey.length==''||proId.length==''||custId.length==''||phoneNum.length===0||dos.value==''||itemNam.length==''||quantity.length===0)
    {
      alert("Please Check all The Details Data Is Empty!!!!")
    }
    else if(!proId.match(/^[0-9a-zA-Z]+$/)){
      alert("Please provide correct product Id")
 
      }
    else if(!custId.match(/^[0-9a-zA-Z]+$/)){
    alert("Please provide correct Customer ID")
 
    }
    else if(!phoneNum.match(/^\d{10}$/)){
      alert("Please provide correct Phone Number")
    
      }
    else if(!itemNam.match(/^[a-zA-Z]+$/)){
        alert("Please provide correct Item Name")
       
    }
    else if(!dos.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/)){
      alert("please check the Date")
    }
    else if(!quantity.match(/^[1-9][0-9]*[a-zA-Z]+$/)){
      alert("Please check the quantity number!!")
    }
    else{
    $.post('/retailProduct',{ Rkey:privKey,productIdR:proId,customerId:custId,phoneNo:phoneNum,itemNameR:itemNam,dos:dos,quantityR:quantity},'json');
    alert("Products Data Are Send!!!");
    document.getElementById('depform').reset();
    return true;
        }
}
