const fetch=require("node-fetch");
const crypto=require("crypto");
const {CryptoFactory, createContext } = require('sawtooth-sdk/signing')
const protobuf = require('sawtooth-sdk/protobuf')
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1') 
const {TextEncoder} = require('text-encoding/lib/encoding')
var encoder =new TextEncoder('utf8');

//Approved keys for producer and retailer
PRODUCERKEY = '0b845ff442ffbba9652ef1078289f7cc0316a54c62649465b36e4f322f89452b'
RETAILERKEY = '4dc1bf10dd9af329419c997fc7c388937049015d61d43e1b8cb23aa9e8b9aa03'

//family name
FAMILY_NAME='Agro_Tracking';


// function to hash data
function hash(data) {
  return crypto.createHash('sha512').update(data).digest('hex');
}

/* function to retrive the address of a particular product based on its productId */

function getProductAddress(productId){
  const context = createContext('secp256k1');
  let key = Secp256k1PrivateKey.fromHex(PRODUCERKEY)
  let signer = new CryptoFactory(context).newSigner(key);
  let publicKeyHex = signer.getPublicKey().asHex()    
  let keyHash  = hash(publicKeyHex)
  let nameHash = hash("Agro_Tracking")
  let proIdHash = hash(productId)
 
  return nameHash.slice(0,6)+proIdHash.slice(0,6)+keyHash.slice(0,58)

}

/* function to create Transaction 
parameter : 
familyName -  the transaction family name 
inputlist - list of input addressess
outputlist - list of output addressess
privkey - the user private key
payload - payload
familyVersion - the version of the family
*/

function createTransaction(familyName,inputList,outputList,Privkey,payload,familyVersion = '1.0'){
  const privateKeyHex = Privkey  
  const context = createContext('secp256k1');
  const secp256k1pk = Secp256k1PrivateKey.fromHex(privateKeyHex.trim());
  signer = new CryptoFactory(context).newSigner(secp256k1pk);
  const payloadBytes = encoder.encode(payload)
  //create transaction header
  const transactionHeaderBytes = protobuf.TransactionHeader.encode({
    familyName: familyName,
    familyVersion: familyVersion,
    inputs: inputList,
    outputs: outputList,
    signerPublicKey: signer.getPublicKey().asHex(),
    nonce: "" + Math.random(),
    batcherPublicKey: signer.getPublicKey().asHex(),
    dependencies: [],
    payloadSha512: hash(payloadBytes),
  }).finish();
 // create transaction
 const transaction = protobuf.Transaction.create({
  header: transactionHeaderBytes,
  headerSignature: signer.sign(transactionHeaderBytes),
  payload: payloadBytes
});

const transactions = [transaction];
  //create batch header
  const  batchHeaderBytes = protobuf.BatchHeader.encode({
    signerPublicKey: signer.getPublicKey().asHex(),
    transactionIds: transactions.map((txn) => txn.headerSignature),
  }).finish();

  const batchSignature = signer.sign(batchHeaderBytes);
  //create batch 
  const batch = protobuf.Batch.create({
    header: batchHeaderBytes,
    headerSignature: batchSignature,
    transactions: transactions,
  });
  //create batchlist
  const batchListBytes = protobuf.BatchList.encode({
    batches: [batch]
  }).finish();

  sendTransaction(batchListBytes);	
}

/*
function to submit the batchListBytes to validator
*/
async function sendTransaction(batchListBytes){

  if (batchListBytes == null) {
    try{
    var geturl = 'http://rest-api:8008/state'+this.address
    console.log("Getting from: " + geturl);
     let response =await fetch(geturl, {
      method: 'GET',
    })
    let responseJson =await response.json();
      var data = responseJson.data;
      var amount = new Buffer(data, 'base64').toString();
      return amount;
    }
    catch(error) {
      console.error(error);
    }	
  }
  else{
    console.log("new code");
    try{
  
  let resp =await fetch('http://rest-api:8008/batches', {
     method: 'POST',
     headers: { 'Content-Type': 'application/octet-stream'},
     body: batchListBytes
     })
        console.log("response", resp);
}
catch(error) {
  console.log("error in fetch", error);

} 
}
}


//class product
class Product{


  /*function to add new product to chain :
parameters:
producer:name of the producer
key:The producer private key
productId:The unique product number
producerId:The unique producer number
itemName: The name of the item
retailerId:The unique number for retailer
dop:Date of Product given for production
doe:The expiary date of the product
quantity:The measure of product sold to the reatailer by producer 
 */
  addProduct(producer,key,productId,producerId,itemName,retailerId,dop,doe,quantity)
      {
        let address=  getProductAddress(productId);
        let action="Add product";
        let payload = [action,producer,productId,producerId,itemName,retailerId,dop,doe,quantity].join(',')
    //  console.log("********************userclient*******************")
    //  console.log("producer********",producer);
     
    // console.log("productId*************",productId);
    // console.log("producerId*************",producerId);
    // console.log("ItemName*************",itemName);
    // console.log("reatilId*************",retailerId);
    // console.log("dop*************",dop);
    // console.log("doe*************",doe);
    // console.log("quantity*************",quantity);

    // console.log("***************************");
    // console.log(payload[0]);
    // console.log(payload[1]);
    // console.log(payload[2]);
    // console.log(payload[3]);
    // console.log(payload[4]);
    // console.log(payload[5]);
    // console.log(payload[6]);
    // console.log(payload[7]);
    // console.log(payload[8]);
    // console.log("***************************");

        if (key == PRODUCERKEY){
        	createTransaction(FAMILY_NAME,[address],[address],key,payload)}
        else{
        	console.log('Producer Not Authorised')
        }

      }
      
       /* function to register the consumer
      parameters :
      Retailer - the one who sells the product to a consumer
      retailerKey - key of retailer
      productIdR - The unique product number
      customerId- The unique number for the customer
      phoneNo- The mobile number of the customer
      dos- the date of sale of product by retailer
      itemNameR: The name of the item
      quantityR- yhe measure of quantity
      */

     retailProduct(Retailer,key,productIdR,customerId,phoneNo,itemNameR,dos,quantityR){
      let action = "Register Details";
      let Address = getProductAddress(productIdR)
      let payload = [action,productIdR,customerId,phoneNo,itemNameR,dos,Retailer,quantityR].join(',')
     console.log("action",action);
  //     console.log("retailer**********",Retailer);
  // console.log("productId*************",productIdR);
  // console.log("custId*************",customerId);
  // console.log("phoneNum*************",phoneNo);
  // console.log("ItemName*************",itemNameR);
  // console.log("dos*************",dos);
  // console.log("quantity*************",quantityR)

  // console.log("***************************");
  //   console.log(payload[0]);
  //   console.log(payload[1]);
  //   console.log(payload[2]);
  //   console.log(payload[3]);
  //   console.log(payload[4]);
  //   console.log(payload[5]);
  //   console.log(payload[6]);
  //   console.log(payload[7]);
  //   console.log("***************************");
      
      if (key == RETAILERKEY){
        createTransaction(FAMILY_NAME,[Address],[Address],key,payload)
      }
      else{
        console.log('Retailer Not Authorised')
      }
      

  }

  //////

/**
 * Get state from the REST API
 * @param {*} address The state address to get
 * @param {*} isQuery Is this an address space query or full address
 */
async getState (address, isQuery) {
  let stateRequest = 'http://rest-api:8008/state';
  if(address) {
    if(isQuery) {
      stateRequest += ('?address=')
    } else {
      stateRequest += ('/address/');
    }
    stateRequest += address;
  }
  let stateResponse = await fetch(stateRequest);
 
  let stateJSON = await stateResponse.json();
  // console.log("stateJSON**************",stateJSON)
  return stateJSON;
}

async getProductListings() {
  let productListingAddress = hash(FAMILY_NAME).substr(0, 6);
  return this.getState(productListingAddress, true);
}

//////

}

module.exports = {Product};


