var express = require('express');
var {Product}=require('./UserClient')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home', { title: 'home' });
});

//get register page
router.get('/register',(req,res,next)=>{
res.render('dashboards',{title: 'Dashboards'})
});
// gets view page
router.get('/tracelist',async(req,res)=>{
var productClient= new Product();
let stateData = await productClient.getProductListings();

console.log("listings******",stateData);

let productList=[];
stateData.data.forEach(products => {
  if(!products.data) return;
  let decodedProducts =Buffer.from(products.data, 'base64').toString();
  let productDetails = decodedProducts.split(',');

  //  console.log(productDetails);
  console.log("decodedProductsList------", decodedProducts);
// views the product in tabular form
  productList.push({
  productId: productDetails[1],
  producerId: productDetails[2],
 itemName: productDetails[3], 
  retailerId: productDetails[4],
  dop:productDetails[5],
  doe:productDetails[6],
  quantity: productDetails[7],
  customerId: productDetails[8],
  phoneNo:productDetails[9],
  quantityOfS: productDetails[13],
  dos: productDetails[11]
});

});



  res.render('tracelist',{ listings: productList})
})





///

router.post('/addProduct',function(req,res){
  let key=req.body.Pkey
  let productId=req.body.productId
  let producerId=req.body.producerId
  let itemName=req.body.itemName
  let retailerId=req.body.retailerId
  let dop=req.body.dop
  let doe=req.body.doe
  let quantity=req.body.quantity

// console.log("***********index.js*********")
// console.log("privKey**********",key);
// console.log("productId*************",productId);
// console.log("producerId*************",producerId);
// // console.log("ItemName*************",itemName);
// console.log("reatilId*************",retailerId);
// console.log("dop*************",dop);
// console.log("doe*************",doe);
// console.log("quantity*************",quantity);


  console.log("Data sent to Rest API");
  var client =new Product();
client.addProduct("Producer",key,productId,producerId,itemName,retailerId,dop,doe,quantity)

res.send({message:"Data added successfully"});

})

router.post('/retailProduct',function(req,res){
  let key=req.body.Rkey
  let productIdR=req.body.productIdR
  let customerId=req.body.customerId
  let phoneNo=req.body.phoneNo
  let itemNameR=req.body.itemNameR
  let dos=req.body.dos
  let quantityR=req.body.quantityR

  // console.log("***********index.js*********")
  // console.log("privKey**********",key);
  // console.log("productId*************",productIdR);
  // console.log("custId*************",customerId);
  // console.log("phoneNum*************",phoneNo);
  // console.log("ItemName*************",itemNameR);
  // console.log("dos*************",dos);
  // console.log("quantity*************",quantityR);
  
  
    console.log("Data sent to REST API");
  var client = new Product();
  client.retailProduct("Retailer",key,productIdR,customerId,phoneNo,itemNameR,dos,quantityR)
 
  console.log("data sent from index");
  res.send({message: "Data successfully added"});

})

module.exports = router;
                                